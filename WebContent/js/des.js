function des(key,val){
	 var keyHex = CryptoJS.enc.Utf8.parse(key);
    var encrypted = CryptoJS.DES.encrypt(val, keyHex, {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7
    });
    //alert(encrypted.toString(), encrypted.ciphertext.toString(CryptoJS.enc.Base64));
	return encrypted.toString();
}
