<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>${pageTitle }</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		
			
		<style type="text/css">
		.easyui-tabs table{
			width:500px;
			margin:10 10 10 10;
		}
		.easyui-tabs table tr .tit{
			width:30%;
		} 
		.easyui-tabs table tr .val{
			width:60%;
		} 
		.easyui-tabs table tr .val *{
			margin:2 3 2 3;
			width:96%;
		} 
		</style>
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.form.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/entity/node.js"></script>
	</head>

	<body style="margin:0;padding:0;text-align:center;"> 
	
	<div class="easyui-layout" fit="true">
   <div region="west" hide="true" split="false" fit="true" id="west">
	<div class="easyui-tabs"  fit="true" border="false" id="fm">
	<div title="基本信息">
    	<table cellpadding="0" cellspacing="0" border="0">
		<c:forEach items="${htmls }" var="html">
			<c:if test="${html.value.k=='base'}">
			<tr>
				<td class="tit">
					<div class="tittext">${html.key }</div>
				</td>
				<td class="val">
					${html.value.v }
				</td>		
			</tr>
			</c:if>
		</c:forEach>
			<tr>
				<td class="tit">
					<div class="tittext">所属站点</div>
				</td>
				<td class="val">
					<select name="subSiteId" mthod="smt">
						<!--option value="-1">独立用户</option-->
						<c:forEach items="${subSiteList }" var="site">
						<option value="${site.id}">${site.siteName}</option>
						</c:forEach>
					</select>
				</td>		
			</tr>
		</table>
    </div>
    </div>
    </div>
    </div>
    </body>
    <script type="text/javascript">
    var path='<%=path%>';
    $(document).ready(function(){
		bindEvents();
	});
	function bindEvents(){
		$("input[smt='formatter']").click(function(e){parent.showFormatterDialog(e);});
		$("input[smt='fileSelecter']").click(function(e){parent.showFileSelecterDialog(e);});
		$("input[smt='userSelecter']").click(function(e){parent.showUserSelecterDialog(e);});
		$("input[smt='templateSelecter']").click(function(e){parent.showTemplateSelecter(e);});
		$("input[smt='dateSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='timeSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='dateTimeSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='colorSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
	};	
	//由于本页面是嵌套在IFRAME中的，所以在展现一些对话框时，无法显示到本页（被挡住了，放不下），所以需要 调用上级页面的对话框显示函数，然后函数再调用下面的这个方法设置值
	/**
	*
	**/
	function setValue(name,val){
		$("[name='"+name+"']").val(val);
		$("[name='"+name+"']").blur();
	};
	
	String.prototype.replaceAll = function(s1,s2) { 
    	return this.replace(new RegExp(s1,"gm"),s2); 
	};
	var rtn=false;
	function submit(){
		var _s_tmp=$("[mthod='smt']");
		var params="";
		for(i=0;i<_s_tmp.size();i++){
			params+=""+(_s_tmp[i].name)+":'"+$(_s_tmp[i]).val()+"',";
		} 
		params="[{"+params+"tmp:''}]";
		$.ajax({
				 type: "POST",
				 url: path+"/Manage/User/doAdd.do",
				 data: eval(params)[0],
				 dataType: "text",
				 async:false,
				 success: function(result){
					if(result.indexOf("msg")!=0){
						var t=eval("("+result+")");
						if(t.status='99'){
							rtn=true;
						}else{
							alert(t.msg);
						}
					}
				}
			 });
		return rtn;
	}
</script>