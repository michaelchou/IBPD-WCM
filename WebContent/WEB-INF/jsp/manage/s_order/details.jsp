<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>${pageTitle }</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript">
		var basePath='<%=path %>';
	</script>
	<style type="text/css">
	#orderInfo{
		width:90%;
		margin:0 auto;
	}
	#orderInfo tr{
		border-bottom:solid 1px red;
		height:18px;
	}
	#orderInfo tr td{
		border-bottom:solid 1px red;
		width:40%;
		height:25px;
	}
	#orderInfo tr td label{
		font-size:12px;
		
	}
	</style>
	</head>

	<body>
	<div class="easyui-layout" fit="true">
		<div region="north" hide="true" split="false" title="订单信息" style="height:200px" id="west">
			<table id="orderInfo" cellpadding="0" cellspacing="0">
				<tr>
				<td>
				<label>订单编号</label>
				</td>
				<td><label>${order.orderNumber}</label>
				</td>
				</tr>
				<tr>
				<td>
				<label>下单会员</label>
				</td>
				<td><label>${order.account}</label>
				</td>
				</tr>
				<tr>
				<td>
				<label>订单状态</label>
				</td>
				<td><label>
				
					<c:if test="${order.status=='file'}">已归档</c:if>
					<c:if test="${order.status=='init'}">已下单</c:if>
					<c:if test="${order.status=='pass'}">已审核</c:if>
					<c:if test="${order.status=='send'}">已发货</c:if>
					<c:if test="${order.status=='sign'}">已签收</c:if>
					<c:if test="${order.status=='cancel'}">已取消</c:if>
				</label>
				</td>
				</tr>
				<tr>
				<td>
				<label>下单时间</label>
				</td>
				<td>
				<label>
				<fmt:formatDate value="${order.createdate}" type="both" dateStyle="default" timeStyle="default"/>
				</label>
				</td>
				</tr>
				<tr>
				<td>
				<label>订单总额</label>
				</td>
				<td><label>${order.ptotal}</label>
				</td>
				</tr>
				<tr>
				<td>
				<label>应付款</label>
				</td>
				<td><label>${order.amount}</label>
				</td>
				</tr>
				<tr>
				<td>
				<label>更新总额</label>
				</td>
				<td><label><c:if test="${order.updateAmount=='y'}">已更新</c:if><c:if test="${order.updateAmount=='n'}">未更新(原价)</c:if></label>
				</td>
				</tr>
				<tr>
				<td>
				<label>付款状态</label>
				</td>
				<td><label><c:if test="${order.paystatus=='y'}">已付款</c:if><c:if test="${order.paystatus=='n'}">未付款</c:if></label>
				</td>
				</tr>
				<tr>
				<td>
				<label>配送方式</label>
				</td>
				<td><label>${order.expressCode}</label>
				</td>
				</tr>
				<tr>
				<td>
				<label>快递名称</label>
				</td>
				<td><label>${order.expressName}</label>
				</td>
				</tr>
				<tr>
				<td>
				<label>订单备注</label>
				</td>
				<td><label>${order.otherRequirement}</label>
				</td>
				</tr>
				<tr>
				<td>
				<label>快递单号</label>
				</td>
				<td><label>${order.expressNo}</label>
				</td>
				</tr>
				<tr>
				<td>
				<label>配送地址</label>
				</td>
				<td><label>${ship.shipaddress}</label>
				</td>
				</tr>
				<tr>
				<td>
				<label>邮编</label>
				</td>
				<td><label>${ship.zip}</label>
				</td>
				</tr>
				<tr>
				<td>
				<label>联系电话</label>
				</td>
				<td><label>${ship.tel}</label>
				</td>
				</tr>
				<tr>
				<td>
				<label>手机</label>
				</td>
				<td><label>${ship.phone}</label>
				</td>
				</tr>
				<tr>
				<td>
				<label>收件人</label>
				</td>
				<td><label>${ship.shipname}</label>
				</td>
				</tr>
				<tr>
				<td>
				<label>退货/款状态</label>
				</td>
				<td>
					<c:if test="${order.refundStatus=='init'}"><label>已申请未审核</label></c:if>
					<c:if test="${order.refundStatus=='unpass'}"><label>审核未通过或未审核</label></c:if>
					<c:if test="${order.refundStatus=='passed'}"><label>审核通过</label></c:if>
					<c:if test="${order.refundStatus=='sendPass'}"><label>买家已发货等待卖家确认</label></c:if>
					<c:if test="${order.refundStatus=='finish'}"><label>完成退货/退款</label></c:if>
				</td>
				</tr>
				<tr>
				<td>
				<label>退款说明</label>
				</td>
				<td><label>${order.refundRemark}</label>
				</td>
				</tr>
				<tr>
				<td>
				<label>积分</label>
				</td>
				<td><label>${order.score}</label>
				</td>
				</tr>
			</table>
		</div>
		<div id="nodeview" region="center" style="background: #eee; overflow-y:hidden;overflow-x:auto;">
		<table id="grid" style="width: 900px;height:auto;" title="商品列表" iconcls="icon-view">            
        </table>
		</div>
 	</div>

	
	<script type="text/javascript">
	$(document).ready(function(){
		InitGrid();//默认没有传参数,参数应该是querydata
	});
 	function reload(){
		$('#grid').datagrid("reload");
	};
	//弹出信息窗口 title:标题 msgString:提示信息 msgType:信息类型 [error,info,question,warning]
	function msgShow(title, msgString, msgType) {
		$.messager.alert(title, msgString, msgType);
	};
	var cmenu;
    function createColumnMenu(){
            cmenu = $('<div/>').appendTo('body');
            cmenu.menu({
                onClick: function(item){
                    if (item.iconCls == 'icon-ok'){
                        $('#grid').datagrid('hideColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-empty'
                        });
                    } else {
                        $('#grid').datagrid('showColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-ok'
                        });
                    }
                }
       		});
            var fields = $('#grid').datagrid('getColumnFields');
            for(var i=0; i<fields.length; i++){
                var field = fields[i];
                var col = $('#grid').datagrid('getColumnOption', field);
                if(col.hidden){
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-empty'
	                });
                }else{
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-ok'
	                });
                }
            }
       };
      function InitGrid(queryData) {
            $('#grid').datagrid({   //定位到Table标签，Table标签的ID是grid
                url: '<%=basePath %>Manage/Order/orderDetailsList.do?id=${orderId }&t='+new Date(), 
                title: '订单详情',
                iconCls: 'icon-search',
                singleSelect:true,
                fit:true,
                width: function () { return document.body.clientWidth * 0.98 },
                nowrap: true,
                striptd:true,
                loadMsg:'数据加载中,请稍后……',
                autoRowHeight: false,
                striped: true,
                collapsible: false,
                pagination: true,
                pageSize: 10,
                pageList: [10,20,30,50,100],
                rownumbers: true,
                //sortName: 'ID',    //根据某个字段给easyUI排序
                sortOrder: 'asc',
                remoteSort: true,
                fitColumns:true,
                idField: 'id',
                queryParams: queryData,  //异步查询的参数 
                onHeaderContextMenu: function(e, field){
                    e.preventDefault();
                    if (!cmenu){
                        createColumnMenu();
                    }
                    cmenu.menu('show', {
                        left:e.pageX,
                        top:e.pageY
                    });
                },
                onRowContextMenu:function(e, rowIndex, rowData){
        			
   				},
   				rowStyler:function(index,row){
   					//if(row.state<=0){
   					//return "background-color:RGB(79,129,189);";
   					//}else{
   					//}
   				},
                columns: [[
                   
					{ title: 'id', field: 'id', width: 10, sortable:true,hidden:true },
					{ title: '商品名称', field: 'productName', width: 50, sortable:true },
					{ title: '购买数量', field: 'number', width: 20, sortable:true },
					{ title: '赠品ID', field: 'giftID', width: 20, sortable:true,hidden:true },
					{ title: '单价', field: 'price', width: 30, sortable:true},
					{ title: '低库存', field: 'lowStocks', width: 20, sortable:true,hidden:true },
					{ title: '规格', field: 'specInfo', width: 40, sortable:true },
					{ title: '商品ID', field: 'productID', width: 20, sortable:true,hidden:true },
					{ title: '允许评论', field: 'isComment', width: 20, sortable:true,hidden:true },
					{ title: '积分', field: 'score', width: 20, sortable:true,hidden:true },
					{ title: '订单ID', field: 'orderID', width: 20, sortable:true,hidden:true },
					{ title: '运费', field: 'fee', width: 30, sortable:true,hidden:true },
					{ title: '总价', field: 'total', width: 30, sortable:true}
               ]], 
                toolbar: [ {
                    id: 'btnReload',
                    text: '刷新',
                    iconCls: 'icon-reload',
                    handler: function () {
                        reload();
                    }
                }],
                onDblClickRow: function (rowIndex, rowData) {
                	
                    $('#grid').datagrid('uncheckAll');
                    $('#grid').datagrid('checkRow', rowIndex);
                    //ShowEditOrViewDialog();
                },
                onClickRow: function (rowIndex, rowData) {
                 }
            })
        };
 			
     </script>
	</body>
</html>
