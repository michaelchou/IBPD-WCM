<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>后台管理 登录</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
	    <script type="text/javascript" src="<%=basePath %>js/jquery-1.4.4.min.js"></script>
		<style type="text/css">
		body{
			padding:0;
			margin:0;
		}
		.bg{
			position:absolute;
			width:100%;
			height:100%;
			z-index:-99;
		}
		.bg img{
			width:100%;
			height:100%;
		}
		.main{
			width:100%;
			height:100%;
			position:absolute;
			z-index:1;
		}
		.main .top{
			float:left;
			width:100%;
			height:30%;
		}
		.main .center{
			padding-top:100px;
			padding-left:170px;
			float:left;
			width:60%;
			height:50%;
			margin:0 auto;
		}
		.main .center .text{
			width:400px;
			height:200px;
		}
		.main .logo{
			width:294px;
			height:64px;
			padding-left:100px;
			padding-top:50px;
		}
		.footer{
			float:left;
			width:60%;
			height:20%;
			margin:0 auto;
			padding-bottom:20px;
			border:solid 1px blue;
		}
		.footer a{
			color:#ffffff;
		}
		.footer a:link {
			color:#ffffff;
			TEXT-DECORATION: none
		}
		.footer a:visited {
			color:#ffffff;
			TEXT-DECORATION: none
		}
		.footer a:hover {
			color:#ffffff;
			text-decoration: underline;
		}
		.footer a:active {
			color:#ffffff;
			text-decoration: underline;
		}
		</style>
	</head>

	<body>
	<div class="main">
		<div class="top"><img class="logo" src="<%=basePath %>images/LOGO.png" /></div>
		<div class="center">
			<img class="text" src="<%=basePath %>images/text.png"/>
			<div class="loginmain"></div>
		</div>
		<div class="footer">
			<a>关于我们</a>
		</div>
	</div>
	<div class="bg">
		<img src="<%=basePath %>images/loginbg/06.jpg"/>
	</div>
	</body>

</html>
