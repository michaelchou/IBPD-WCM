<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>添加新表单</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<style type="text/css">
	.cnt span{
		font-size:12px;
	}
	.ope input{
		text-align:center;
	}
	</style>
	</head>

	<body>
	<div>
		<div class="cnt">
           <span><input type="radio" name="slt" value="1">使用默认表单</span>
           <span><input type="radio" name="slt" value="2">新建全新表单</span>
           <span><input type="radio" name="slt" value="3">引用现有表单</span>
           <span><select id="formListSelect"><option id="-1">--不选择--</option></select></span>
		</div>
		<div class="ope">
			<input type="button" value="执行"/>
		</div>
	</div>
	
	<script type="text/javascript">
	var path="<%=path %>";
	var type="${type }";
	var nodeId="${nodeId }";
	
		function initFormList(){
			$.post(
				path+'/Manage/NodeForm/getFormList.do',
				{"nodeId":nodeId,"type":type},
				function(result){
					var r=eval(""+result+"");
					for(i=0;i<r.length;i++){
						$("<option id='"+r[i].id+"'>"+r[i].formName+"</option>").appendTo($("#formListSelect"));
					}
				}
			); 
		};
		var addType="";
		function radioClick(e){
			var id=$(e.target).val();
			if(id=="1"){
				$("#formListSelect").hide();
				addType="useDefault";
			}else if(id=="2"){
				$("#formListSelect").hide();
				addType="new";
			}else if(id=="3"){
				$("#formListSelect").show();
				addType="link";
			}
		};
		function submit(){
		if(addType==""){
			$.messager.alert("提示","请选择选项","warning");
		}
			$.post(
				path+'/Manage/NodeForm/doAdd.do',
				{"nodeId":nodeId,"type":type,"addType":addType,"formId":$("#formListSelect").children("option:selected").attr("id")},
				function(result){
					var r=eval("("+result+")");
					if(r.status=="99"){
						parent.reInit();
					}else{
						$messager.alert("",r.msg,"error");
					}
				}
			); 
		};
        $(function(){
			initFormList();
			$("input[type='radio']").click(radioClick);
			$("input[type='button']").click(submit);
			$("#formListSelect").hide();
        });
	</script>
	</body>
</html>
