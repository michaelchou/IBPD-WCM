<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>栏目管理</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
		<style type="text/css">
		body{
			overflow-y:auto;
			overflow-x:hidden;
			margin:0;
			padding:0;
		}
		.baseInfo{
			width:100%;
			
		}
		.baseInfo tr .tit .tittext{
			font-size:10px;
			overflow:hidden;
			text-overflow:clip;
			width:60px;
		}
		.baseInfo tr .val{
			padding:2 2 2 2;
			width:110px;
			border-bottom:solid 1px #464646;
		}
		.baseInfo tr .val .int {
			width:76px;
		}
		.baseInfo tr .val .int input{
			width:76px;
		}
		.baseInfo tr .val .int select{
			width:76px;
		}
		.baseInfo tr .val .int textarea{
			width:76px;
		}
		.baseInfo tr .val .assist {
			width:20px;
		}
		.baseInfo tr .val .assist input {
			width:20px;
		}
		/*
		.hidden20150323{
			display:none;
		}*/
		</style>
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	</head>

	<body>
		<div style="height:98%;width:100%;overflow-y:auto;">
		<input type="hidden" id="nodeId" value="${node.id }"/>
			
				<table class="baseInfo" cellpadding="0" cellspacing="0">
				<c:forEach items="${fields }" var="field">
					<tr <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>>
						<th class="tit"<c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>><div class="tittext">${field.displayName }</div></th>
						<td class="val"><!--fieldName,displayName,fieldDescription,expGroup,optionalValue,defaultValue,tagType,isEditable,writeByAddMode,displayByAddMode,writeByEditMode,displayByEditMode,assistTags-->
							
							<c:if test="${field.tagType=='0' && field.displayByEditMode==true }">
							<span class="int"><input type="text" mthod="smt" name="${field.fieldName }" value="${node[field.fieldName] }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>/></span>
							</c:if>
							<c:if test="${field.tagType=='1' && field.displayByEditMode==true }">
							<span class="int"><input type="text" mthod="smt" name="${field.fieldName }" value="${node[field.fieldName] }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>/></span>
							</c:if>
							<c:if test="${field.tagType=='2' && field.displayByEditMode==true }">
							<span class="int"><textarea mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>>${node[field.fieldName] }</textarea></span>
							</c:if>
							<c:if test="${field.tagType=='3' && field.displayByEditMode==true }">
							<span class="int">
							<select mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${node[field.fieldName]==v[0] }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							<c:if test="${field.tagType=='4' && field.displayByEditMode==true }">
							<span class="int">
							<select mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${node[field.fieldName]==v[0] }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							<c:if test="${field.tagType=='5' && field.displayByEditMode==true }">
							<span class="int">
							<select mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByEditMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${node[field.fieldName]==v[0] }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							</span>
							<!--辅助控件部分-->
							<c:if test="${field.assistTags!='none' && field.assistTags!='' && field.displayByEditMode==true  }"><span class="assist"></c:if>
							<c:if test="${field.assistTags=='formatter' }">
								<input type="button" smt="formatter" name="${field.fieldName }_formatter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='fileSelecter' }">
								<input type="button" smt="fileSelecter" name="${field.fieldName }_fileSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='userSelecter' }">
								<input type="button" smt="userSelecter" name="${field.fieldName }_userSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='templateSelecter' }">
								<input type="button" smt="templateSelecter" name="${field.fieldName }_templateSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='dateSelecter' }">
								<input type="button" smt="dateSelecter" name="${field.fieldName }_dateSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='timeSelecter' }">
								<input type="button" smt="timeSelecter" name="${field.fieldName }_timeSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='dateTimeSelecter' }">
								<input type="button" smt="dateTimeSelecter" name="${field.fieldName }_dateTimeSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='colorSelecter' }">
								<input type="button" smt="colorSelecter" name="${field.fieldName }_colorSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags!='none' && field.assistTags!='' && field.displayByEditMode==true  }"></span></c:if>
						</td>
						
					</tr>
				</c:forEach>
					<tr class="hidden20150323">
						<th class="tit"><div class="tittext">导航中显示</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="attr_displayInNavigator">
								<option value="true" <c:if test="${attr.displayInNavigator==true }">selected</c:if>>显示</option>
								<option value="false" <c:if test="${attr.displayInNavigator==false }">selected</c:if>>隐藏</option>
							</select>
							</span>
						</td>
					</tr>
					<tr class="hidden20150323">
						<th class="tit"><div class="tittext">导航排序</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="attr_orderByNavigator" value="${attr.orderByNavigator }"/>
							</span>
						</td>
						</tr>
					<tr class="hidden20150323">
						<th class="tit"><div class="tittext">存放目录</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="attr_directory" value="${attr.directory }"/>
							</span>
						</td>
					</tr>
					<tr class="hidden20150323">
						<th class="tit"><div class="tittext">栏目页面模板设置</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="attr_nodePageTemplateId">
								<option value="-1" <c:if test="${attr.nodePageTemplateId==-1 }">selected</c:if>>使用上级设置</option>
								<c:forEach items="${nodePageTemplateList }" var="pt">
								<option value="${pt.pageTemplate.id }" <c:if test="${pt.pageTemplate.id==attr.nodePageTemplateId }">selected</c:if>>${pt.pageTemplate.title }</option>
								</c:forEach>
							</select>
							</span>
						</td>
					</tr>
					<tr class="hidden20150323">
						<th class="tit"><div class="tittext">栏目样式模板设置</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="attr_nodeStyleTemplateId">
								<option value="-1" <c:if test="${attr.nodeStyleTemplateId==-1 }">selected</c:if>>使用上级设置</option>
								<c:forEach items="${nodeStyleTemplateList }" var="pt">
								<option value="${pt.id }" <c:if test="${pt.id==attr.nodeStyleTemplateId }">selected</c:if>>${pt.title }</option>
								</c:forEach>
							</select>
							</span>
						</td>
					</tr>
					<tr class="hidden20150323">
						<th class="tit"><div class="tittext">内容页面模板设置</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="attr_contentpageTemplateId">
								<option value="-1" <c:if test="${attr.contentpageTemplateId==-1 }">selected</c:if>>使用上级设置</option>
								<c:forEach items="${contentpageTemplateList }" var="pt">
								<option value="${pt.pageTemplate.id }" <c:if test="${pt.pageTemplate.id==attr.contentpageTemplateId }">selected</c:if>>${pt.pageTemplate.title }</option>
								</c:forEach>
							</select>
							</span>
						</td>
					</tr>
					<tr class="hidden20150323">
						<th class="tit"><div class="tittext">内容样式模板设置</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="attr_contentStyleTemplateId">
								<option value="-1" <c:if test="${attr.contentStyleTemplateId==-1 }">selected</c:if>>使用上级设置</option>
								<c:forEach items="${contentStyleTemplateList }" var="pt">
								<option value="${pt.id }" <c:if test="${pt.id==attr.contentStyleTemplateId }">selected</c:if>>${pt.title }</option>
								</c:forEach>
							</select>
							</span>
						</td>
					</tr>
					<tr class="hidden20150323">
						<th class="tit"><div class="tittext">评论页面模板设置</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="attr_commentpageTemplateId">
								<option value="-1" <c:if test="${attr.commentpageTemplateId==-1 }">selected</c:if>>使用上级设置</option>
								<c:forEach items="${commentpageTemplateList }" var="pt">
								<option value="${pt.pageTemplate.id }" <c:if test="${pt.pageTemplate.id==attr.commentpageTemplateId }">selected</c:if>>${pt.pageTemplate.title }</option>
								</c:forEach>
							</select>
							</span>
						</td>
					</tr>
					<tr class="hidden20150323">
						<th class="tit"><div class="tittext">评论样式模板设置</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="attr_commentStyleTemplateId">
								<option value="-1" <c:if test="${attr.commentStyleTemplateId==-1 }">selected</c:if>>使用上级设置</option>
								<c:forEach items="${commentStyleTemplateList }" var="pt">
								<option value="${pt.id }" <c:if test="${pt.id==attr.commentStyleTemplateId }">selected</c:if>>${pt.title }</option>
								</c:forEach>
							</select>
							</span>
						</td>
					</tr>
				</table>
</div>
	
	<script type="text/javascript">
	var path="<%=path %>";
	$(document).ready(function(){
		bindEvents();
		<c:if test="${permissionEnable==true}">
		$("[mthod='smt']").bind("blur",function(e){
			var nodeId=$("#nodeId").val();
			if(e.currentTarget.tagName=="SELECT"){
				var val=$(e.currentTarget).val();//$(e.currentTarget).children("option:selected").attr("value");
			}else{
				var val=e.currentTarget.value;
			}
			
			$.post(
				path+"/Manage/Node/saveProp.do",
				{id:nodeId,field:e.currentTarget.name,value:val},
				function(result){
					
				}
			);
		});
		</c:if>
	});   
	function bindEvents(){
		var nodeId=$("#nodeId").val();
		$("input[smt='formatter']").click(function(e){parent.showFormatterDialog(e,nodeId);});
		$("input[smt='fileSelecter']").click(function(e){selectFile(e,nodeId);});
		$("input[smt='userSelecter']").click(function(e){parent.showUserSelecterDialog(e,nodeId);});
		$("input[smt='templateSelecter']").click(function(e){parent.showTemplateSelecter(e,nodeId);});
		$("input[smt='dateSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='timeSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='dateTimeSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='colorSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
	};	
	function selectFile(e,nodeId){
		curImageId=$(e.target).attr("name").split("_")[0];
		window.open(path+'/Manage/FileSelecter/index.do?nodeId='+nodeId+'&filter='+curImageId,'文件上传','toolbar=no,menubar=no,resizable=no,location=no,status=no,z-look:yes');
	};
	function changeFile(s,f){
		if(s!=""){
			s=s.substring(0,s.length-1);
		}
		if($("input[name='"+curImageId+"_fileAppend']:checked").length>0){
			$("input[name='"+f+"']").val($("input[name='"+f+"']").val()+";"+s);
		}else{
			$("input[name='"+f+"']").val(s);
		}
       
	};
	//由于本页面是嵌套在IFRAME中的，所以在展现一些对话框时，无法显示到本页（被挡住了，放不下），所以需要 调用上级页面的对话框显示函数，然后函数再调用下面的这个方法设置值
	/**
	*上级页面调用这个函数来做到设置本页 tag值的效果   参数：name为本页tag的name值，val为具体值,具体调用效果可参阅node的index.jsp页面
	**/
	function setValue(name,val){
		$("[name='"+name+"']").val(val);
		$("[name='"+name+"']").blur();
	};
     </script>
     
	</body>
</html>
