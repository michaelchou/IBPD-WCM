<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>${pageTitle}</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/poshytip-1.2/src/tip-yellow/tip-yellow.css" />
		
			
		<style type="text/css">
		.easyui-tabs table{
			width:450px;
			margin:10 10 10 10;
		}
		.easyui-tabs table tr .tit{
			width:150px;
			padding:2 5 2 5;
		} 
		.easyui-tabs table tr .val{
			width:200px;
			padding:2 5 2 5;
		} 
		.easyui-tabs table tr .val input{
			width:200px;
		} 
		.easyui-tabs table tr .val select{
			width:200px;
		} 
		/*
		.hidden20150323{
			display:none;
		}*/
		</style>
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.validatebox.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/extValidatebox.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/poshytip-1.2/src/jquery.poshytip.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/datese/lhgcore.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/datese/lhgcalendar.js"></script>
	</head>

	<body style="margin:0;padding:0;text-align:center;">
	
	<div class="easyui-layout" fit="true">
   <div region="west" hide="true" split="false" fit="true" id="west">
		<p>本功能支持导入windows下编码为GB2312的CSV格式的文本信息,倒入格式为:栏目名称,是否锁定(y为不锁定n为锁定),导航中是否显示(y为显示n为不显示),链接地址,每个栏目中间以回车符分割,也可以事先再excel中编辑好保存为CSV格式。所有导入字段中不能有“逗号”字符,如果确实需要该字符,请以#dot#代替.</p>
		<br/>
		<form method="post" enctype="Multipart/form-data" action="<%=path%>/Manage/Node/doImport.do?nodeId=${nodeId}">
			<input type="file" name="file"/><input type="submit" value="上传"/>
		</form>
    </div>
    </div>
    </body>
    <script type="text/javascript">
    var path='<%=path%>';
    $(document).ready(function(){
	});
	String.prototype.replaceAll = function(s1,s2) { 
    	return this.replace(new RegExp(s1,"gm"),s2); 
	};
	var rtn=false;
	function submit(){
		var _s_tmp=$("[data-options]");
		var flog=true;
		for(i=0;i<_s_tmp.size();i++){
			if(!$(_s_tmp[i]).validatebox("isValid")){
				flog=false;
				break;
			}
		} 
		if(!flog){
			return false;
		}
		var _s_tmp=$("[mthod='smt']");
		var params="";
		for(i=0;i<_s_tmp.size();i++){
			params+="'oMap[\""+$(_s_tmp[i]).attr("orderNodeId")+"\"]':'"+$(_s_tmp[i]).val()+"',";
		} 
		params="[{"+params+"'"+Math.ceil(Math.random()*999999)+"':'0'}]";
		//alert(params);
			$.ajax({
				 type: "POST",
				 url: path+"/Manage/Node/doOrder.do?t="+Math.ceil(Math.random()*999999),
				 data: eval(params)[0],
				 dataType: "text",
				 cache:false,
				 async:false,
				 success: function(result){
					if(result.indexOf("msg")!=0){
						var t=eval("("+result+")");
						if(t.status='99'){
							rtn=true;
						}else{
							alert(t.msg);
						}
					}
				}
			 });
		return rtn;
	}
</script>