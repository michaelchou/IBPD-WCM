<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>${pageTitle }</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
			
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript">
		var basePath='<%=path %>';
	</script>
	</head>

	<body>
	<div class="easyui-layout" fit="true">
    <div id="nodeview" region="center" style="background: #eee; overflow-y:hidden">
        <div id="tabs" tabPosition="bottom" class="easyui-tabs"  fit="true" border="false" >
 			<div title="分类管理" style="padding:0;overflow:hidden; color:red; " >
			     <table id="grid" style="width: 900px;height:auto;" title="分类信息" iconcls="icon-view">            
           		 </table>
			
			</div>
			   <c:if test="${loginedUserType=='manage'}">
			<div title="分类参数" style="overflow:hidden; color:red; " >
			    
			</div>
				</c:if>
			   <c:if test="${loginedUserType=='manage'}">
			<div title="分类属性" style="padding:0;overflow:hidden; color:red; " >
			</div>
				</c:if>
			<div title="商品管理" style="padding:0px;overflow:hidden; color:red; " >
			</div>

		</div>
    </div>	   
    <div region="east" hide="true" split="false" title="属性配置" style="width:230px;" id="west">
	    <iframe id="propsPanel" onreadystatechange="resize()"  src="<%=path %>/Manage/Catalog/props.do?id=-1" scrolling="auto"  frameborder="0" style="width:100%;height:100%;"></iframe>
    </div>
	</div>

	<div id="nodeCtxMenu" class="easyui-menu" style="width:130px;">
	    <div onClick="ShowEditOrViewDialog()" data-options="iconCls:'icon-edit'">编辑该栏目</div>
	    <div onClick="del()" data-options="iconCls:'icon-remove'">删除该栏目</div>
	    <div onClick="reload()" data-options="iconCls:'icon-reload'">刷新栏目列表</div>
	    <div onClick="publish_node()" data-options="iconCls:'icon-publish-node'">发布该栏目首页</div>
	    <div onClick="publish_content()" data-options="iconCls:'icon-publish-content'">发布该栏目内容</div>
	    <div onClick="publish_tree()" data-options="iconCls:'icon-publish-tree'">增量发布</div>
	    <div onClick="closeNode()" data-options="iconCls:'icon-closeNode'">开启/关闭该栏目</div>
	    <div onClick="hiddenToNav()" data-options="iconCls:'icon-hiddenToNav'">导航显示显示/隐藏</div>
	</div>
	
	<script type="text/javascript">
	$(document).ready(function(){
		initTabs();
		InitCatalogGrid();//默认没有传参数,参数应该是querydata
	});
	 var oTime = null;
    function resize()
    {
        if(oTime)
        {
            clearTimeout(oTime);
        }
         
        oTime = setTimeout(reset, 200);
    }
     
    function reset()
    {
        var frame = document.getElementById("propsPanel");
        var outHeight = frame.offsetHeight;
        var inHeight = frame.contentWindow.document.body.scrollHeight;
        if(outHeight != inHeight)
        {
            frame.style.height = (inHeight + 10) + "px";
        }
    }

	function initTabs(){
	$("#tabs").tabs({
		onSelect:function(title){
			var url='';
			var paramIframe='<iframe src="<%=path %>/Manage/Catalog/params.do?id=${cataId }&type=0" height="100%" width="100%" scrolling="auto"  frameborder="0" ></iframe>';
			var attributeIframe='<iframe src="<%=path %>/Manage/Catalog/params.do?id=${cataId }&type=1" height="100%" width="100%" scrolling="auto"  frameborder="0" ></iframe>';
			var productIframe='<iframe src="<%=path %>/Manage/Product/index.do?catalogId=${cataId }" height="100%" width="100%" scrolling="auto"  frameborder="0" ></iframe>';
			
			var assest='';
			var customTag='';
			if(title=='分类参数'){
				url=paramIframe;
			}else if(title=='分类属性'){
				url=attributeIframe;
			}else if(title=='商品管理'){
				url=productIframe;
			}
			var tab = $('#tabs').tabs('getSelected');
			$("#tabs").tabs('update',{  
				tab:tab,  
				options:{  
					title:title,  
					style:{padding:'1px'},  
					//href:URL, // 使用href会导致页面加载两次，所以使用content代替  
					content:url,  
					closable:false,  
					fit:true,  
					selected:true  
				}
			});
		}
	});
	};
	function hiddenToNav(){
		var _setNode=$('#grid').datagrid("getSelected");
		$.post(
					basePath+"/Manage/Catalog/navi.do",
					{id:_setNode.id},
					function(result){
						msgShow("提示","执行成功.","warning");
						reload();
					}
				);

	};
	//not used function
	function closeNode(){
		var _setNode=$('#grid').datagrid("getSelected");
		$.post(
					basePath+"/Manage/Catalog/state.do",
					{id:_setNode.id},
					function(result){
						//msgShow("提示","锁定成功.","warning");
						$('#grid').datagrid("reload");
					}
				);
	};
	function reload(){
		$('#grid').datagrid("reload");
	};
	function del(){
		var _sets=$('#grid').datagrid("getSelections");
		if(_sets.size==0){
			msgShow("提示","没有选中行","warning");
		}else{
			var _ids="";
			$.each(_sets,function(i,n){
			_ids+=n.id+",";
			});
		$.messager.confirm("确认","如果分类下已经有商品,删除分类将会同时将该分类下商品全部删除,确定删除吗?",function(r){
			if(r){
				$.post(
					basePath+"/Manage/Catalog/doDel.do",
					{ids:_ids},
					function(result){
						msgShow("提示","删除成功.","warning");
						reload();
					}
				);
			}
		});
		}
	};
	//弹出信息窗口 title:标题 msgString:提示信息 msgType:信息类型 [error,info,question,warning]
	function msgShow(title, msgString, msgType) {
		$.messager.alert(title, msgString, msgType);
	};
	var cmenu;
    function createColumnMenu(){
            cmenu = $('<div/>').appendTo('body');
            cmenu.menu({
                onClick: function(item){
                    if (item.iconCls == 'icon-ok'){
                        $('#grid').datagrid('hideColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-empty'
                        });
                    } else {
                        $('#grid').datagrid('showColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-ok'
                        });
                    }
                }
       		});
            var fields = $('#grid').datagrid('getColumnFields');
            for(var i=0; i<fields.length; i++){
                var field = fields[i];
                var col = $('#grid').datagrid('getColumnOption', field);
                if(col.hidden){
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-empty'
	                });
                }else{
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-ok'
	                });
                }
            }
       };
      function InitCatalogGrid(queryData) {
            $('#grid').datagrid({   //定位到Table标签，Table标签的ID是grid
                url: '<%=basePath %>Manage/Catalog/list.do?id=${cataId }&t='+new Date(), 
                title: '',
                iconCls: 'icon-grid',
                singleSelect:false,
                fit:true,
                width: function () { return document.body.clientWidth * 0.98 },
                nowrap: true,
                striptd:true,
                loadMsg:'数据加载中,请稍后……',
                autoRowHeight: false,
                striped: true,
                collapsible: false,
                pagination: true,
                pageSize: 10,
                pageList: [10,20,30,50,100],
                rownumbers: true,
                //sortName: 'ID',    //根据某个字段给easyUI排序
                sortOrder: 'asc',
                remoteSort: true,
                fitColumns:true,
                idField: 'id',
                queryParams: queryData,  //异步查询的参数 
                onHeaderContextMenu: function(e, field){
                    e.preventDefault();
                    if (!cmenu){
                        createColumnMenu();
                    }
                    cmenu.menu('show', {
                        left:e.pageX,
                        top:e.pageY
                    });
                },
                onRowContextMenu:function(e, rowIndex, rowData){
        			//e.preventDefault();
        			//$('#grid').datagrid('uncheckAll');
                   // $('#grid').datagrid('checkRow', rowIndex);
				   // $('#nodeCtxMenu').menu('show', {
				   //     left:e.pageX,
				   //     top:e.pageY
				   // });    
   				},
   				rowStyler:function(index,row){
   					//if(row.state<=0){
   					//return "background-color:RGB(79,129,189);";
   					//}else{
   					//}
   				},
                columns: [[
                    { field: 'ck', checkbox: true,title:'选择' },   //选择
					{ title: 'ID', field: 'id', width: 20, sortable:true,hidden:true },
					{ title: '分类名称', field: 'name', width: 100, sortable:true },
					{ title: '分类', field: 'type', width: 50, sortable:true,hidden:true },
					{ title: '编码', field: 'code', width: 50, sortable:true},
					{ title: '导航显示', field: 'showInNav', width: 30, sortable:true,formatter:function(val, rowdata, index){if(val=='y'){return "显示";}else{return "隐藏";}}},
					{ title: '上级ID', field: 'pid', width: 60, sortable:true,hidden:true },
					{ title: '排序', field: 'order', width: 20, sortable:true },
					{ title: '存在下级', field: 'leaf', width: 60, sortable:true,hidden:true,formatter:function(val, rowdata, index){if(val==true){return "否";}else{return "是";}} }
               ]], 
			   <c:if test="${loginedUserType=='manage'}">
                 toolbar: [{
                    id: 'btnAdd',
                    text: '添加',
                    iconCls: 'icon-add',
                    handler: function () {
                        ShowAddDialog();
                    }
                },{
                    id: 'btnEdit',
                    text: '修改',
                    iconCls: 'icon-edit',
                    handler: function () {
                        ShowEditDialog();//实现修改记录的方法
                    }
                }, '-', {
                    id: 'btnDelete',
                    text: '删除',
                    iconCls: 'icon-remove',
                    handler: function () {
                        del();//实现直接删除数据的方法
                    }
                }, '-', {
                    id: 'btnReload',
                    text: '刷新',
                    iconCls: 'icon-reload',
                    handler: function () {
                        reload();
                    }
                }],
				</c:if>
			   <c:if test="${loginedUserType=='tenant'}">
			    toolbar: [{
                    id: 'btnReload',
                    text: '刷新',
                    iconCls: 'icon-reload',
                    handler: function () {
                        reload();
                    }
                }],
				
				</c:if>
                onDblClickRow: function (rowIndex, rowData) {
                	
                    $('#grid').datagrid('uncheckAll');
                    $('#grid').datagrid('checkRow', rowIndex);
                    //ShowEditOrViewDialog();
                },
                onClickRow: function (rowIndex, rowData) {
                	loadProps(rowData.id);
                    //$('#grid').datagrid('uncheckAll');
                    //$('#grid').datagrid('checkRow', rowIndex);
                    //ShowEditOrViewDialog();
                }
            })
        };
        function ShowEditDialog(nId){
        var editDialog = $('<div id="editNodeDiv"/>').appendTo('body');
        var nodeId="-1";
        if(nId==null){
	        var node=$('#grid').datagrid("getSelected");
	        nodeId=node.id;
        }else{
        	nodeId=nId;
        }
        $(editDialog).dialog({
        	modal:true,
        	title:'更新',
        	shadow:true,
        	iconCls:'icon-edit',
        	width:600,
        	height:500,
        	resizable:true,
        	toolbar:[{
                    text:'保存',
                    iconCls:'icon-save',
                    handler:function(){
                        if($("#editiframe")[0].contentWindow.submit()){
	                        $(editDialog).dialog("close");
	                        $(editDialog).remove();
							reload();
                        }else{
                        }
                    }
                },'-',{
                    text:'取消',
                    iconCls:'icon-cancel',
                    handler:function(){
                         $(editDialog).dialog("close");
	                     $(editDialog).remove();
                    }
                }],
        	content:'<iframe id="editiframe" width="500px" height="455px" scrolling="no" frameborder="no" style="overflow:hidden;" src="'+basePath+'/Manage/Catalog/toEdit.do?id='+nodeId+'&t='+new Date()+'"></iframe>'
        });
        $(editDialog).dialog("open");
        };
        function ShowAddDialog(nId){
        var addDialog = $('<div id="addNodeDiv"/>').appendTo('body');
        var nodeId="-1";
        if(nId==null){
	        nodeId="<%=request.getParameter("id") %>";
        }else{
        	nodeId=nId;
        }
        $(addDialog).dialog({
        	modal:true,
        	title:'添加',
        	shadow:true,
        	iconCls:'icon-add',
        	width:600,
        	height:500,
        	resizable:true,
        	toolbar:[{
                    text:'保存',
                    iconCls:'icon-save',
                    handler:function(){
                        if($("#addiframe")[0].contentWindow.submit()){
	                        $(addDialog).dialog("close");
	                        $(addDialog).remove();
							reload();
                        }else{
                        }
                    }
                },'-',{
                    text:'取消',
                    iconCls:'icon-cancel',
                    handler:function(){
                         $(addDialog).dialog("close");
	                     $(addDialog).remove();
                    }
                }],
        	content:'<iframe id="addiframe" width="585px" height="455px" scrolling="no" frameborder="no" style="overflow:hidden;" src="'+basePath+'/Manage/Catalog/toAdd.do?parentId='+nodeId+'&t='+new Date()+'"></iframe>'
        });
        $(addDialog).dialog("open");
        };
        //下面是用来测试属性配置部分的
        function loadProps(id){
        $("#propsPanel").attr("src","<%=path %>/Manage/Catalog/props.do?id="+id+"&t="+new Date());
        };
			
     </script>
	</body>
</html>
