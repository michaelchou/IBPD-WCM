package com.ibpd.henuocms.entity;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ibpd.entity.baseEntity.IBaseEntity;

/**
 * 样式模板
 * @author MG
 * @version 1.0
 */
@Entity
@Table(name="T_StyleTemplate")
public class StyleTemplateEntity extends IBaseEntity{
	public static final Integer TEMPLATE_TYPE_SITE=2;
	public static final Integer TEMPLATE_TYPE_NODE=1;
	public static final Integer TEMPLATE_TYPE_CONTENT=0;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	/** 
	 * 标题
	 */
	@Column(name="f_title",length=50,nullable=true)
	private String title=" ";
	/**
	 * 所属站点ID
	 */
	@Column(name="f_subSiteId",nullable=true)
	private Long subSiteId=Long.parseLong("-1");
	/**
	 * 模板类别
	 */
	@Column(name="f_type",nullable=true)
	private Integer type=0;
	/**
	 * 样式模板路径
	 */
	@Column(name="f_templateFilePath",length=500,nullable=true)
	private String templateFilePath=" ";
	/**
	 * 创建日期
	 */
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_createDate",nullable=true)
	private Date createDate=new Date();
	/**
	 * 最后更新时间

	 */
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_lastUpateDate",nullable=true)
	private Date lastUpateDate=new Date();
	/**
	 * 备注
	 */
	@Column(name="f_remark",length=50,nullable=true)
	private String remark=" ";
	/**
	 * 文件大小
	 */
	@Column(name="f_size",nullable=true)
	private Integer size=0;
	/**
	 * 附件大小（整个文件夹大小）

	 */
	@Column(name="f_folderSize",nullable=true)
	private Integer folderSize=0;
	/**
	 * 分组
	 */
	@Column(name="f_group",length=50,nullable=true)
	private String group=" ";
	/**
	 * 排序
	 */
	@Column(name="f_order",nullable=true)
	private Integer order=0;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Long getSubSiteId() {
		return subSiteId;
	}
	public void setSubSiteId(Long subSiteId) {
		this.subSiteId = subSiteId;
	}
	public String getTemplateFilePath() {
		return templateFilePath;
	}
	public void setTemplateFilePath(String templateFilePath) {
		this.templateFilePath = templateFilePath;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getLastUpateDate() {
		return lastUpateDate;
	}
	public void setLastUpateDate(Date lastUpateDate) {
		this.lastUpateDate = lastUpateDate;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}
	public Integer getFolderSize() {
		return folderSize;
	}
	public void setFolderSize(Integer folderSize) {
		this.folderSize = folderSize;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public Integer getOrder() {
		return order;
	}
	public void setOrder(Integer order) {
		this.order = order;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getType() {
		return type;
	}
	@Override
	public String toString() {
		return "StyleTemplateEntity [createDate=" + createDate
				+ ", folderSize=" + folderSize + ", group=" + group + ", id="
				+ id + ", lastUpateDate=" + lastUpateDate + ", order=" + order
				+ ", remark=" + remark + ", size=" + size + ", subSiteId="
				+ subSiteId + ", templateFilePath=" + templateFilePath
				+ ", title=" + title + ", type=" + type + "]";
	}
	
}