package com.ibpd.henuocms.service.user;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.henuocms.entity.UserEntity;
@Transactional
@Service("userService")
public class UserServiceImpl extends BaseServiceImpl<UserEntity> implements IUserService {
	public UserServiceImpl(){
		super();
		this.tableName="UserEntity";
		this.currentClass=UserEntity.class;
		this.initOK();
	}
 
	public UserEntity getUserByUserName(String userName) {
		UserEntity ue=null;
		List<UserEntity> l=getList("from "+getTableName()+" where userName='"+userName+"'",null);
		if(l.size()>0){
			ue=l.get(0);
		}
		return ue;
	}

	public boolean checkUserExist(String userName) {
		List<UserEntity> l=getList("from "+getTableName()+" where userName='"+userName+"'",null);
		if(l==null || l.size()==0)
			return false;
		else
			return true;
	}
	
}
