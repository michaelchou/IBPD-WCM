package com.ibpd.henuocms.service.field;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.henuocms.entity.FieldEntity;
@Transactional
@Service("fieldService")
public class FieldServiceImpl extends BaseServiceImpl<FieldEntity> implements IFieldService {
	public FieldServiceImpl(){
		super();
		this.tableName="FieldEntity";
		this.currentClass=FieldEntity.class;
		this.initOK();
	}
	
}
 