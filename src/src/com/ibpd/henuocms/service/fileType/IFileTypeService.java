package com.ibpd.henuocms.service.fileType;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.henuocms.entity.FileTypeEntity;

public interface IFileTypeService extends IBaseService<FileTypeEntity> {

}
 