package com.ibpd.henuocms.service.model;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.henuocms.entity.ModelEntity;

public interface IModelService extends IBaseService<ModelEntity> {

}
 