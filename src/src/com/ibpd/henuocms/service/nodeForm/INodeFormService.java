package com.ibpd.henuocms.service.nodeForm;

import java.util.List;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.henuocms.entity.NodeFormEntity;
import com.ibpd.henuocms.entity.NodeFormFieldEntity;
import com.ibpd.henuocms.entity.ext.NodeFormExtEntity;

public interface INodeFormService extends IBaseService<NodeFormEntity> {
	/**
	 * 初始化默认的栏目表单，注意：初始化的时候会删除 原先初始化的数据
	 */
	void initDefaultNodeForm();
	/**
	 * 初始化默认的内容表单，注意：初始化的时候会删除 原先初始化的数据
	 */
	void initDefaultContentForm();
	/**
	 * 设置栏目的表单,其实就是设置NodeNodeForm的信息
	 * @param nodeId
	 * @param nodeFormId 
	 */
	void linkNodeForm(Long nodeId,Long nodeFormId);
	/**
	 * 将设置的栏目表单解绑,其实就是设置NodeNodeForm的信息
	 * @param nodeId
	 * @param nodeFormId
	 */
	void UnLinkNodeForm(Long nodeId,Long nodeFormId);
	void UnLinkNodeForm(Long nodeId,Integer type);
	/**
	 * 活取栏目表单ID
	 * @param formType
	 * @param id
	 * @return
	 */
	Long getNodeFormId(Integer formType,Long id);
	/**
	 * 获取栏目表单的扩展，包括表单字段
	 * @param formType
	 * @param id
	 * @return
	 */
	NodeFormExtEntity getNodeForm(Integer formType,Long nodeId,Long formId);
	/**
	 * 获取form列表，尚未扩展根据站点显示
	 * @param pageIndex
	 * @param pageSize
	 * @param orderBy
	 * @param filter where 后面的hql语句
	 * @return
	 */
	List<NodeFormEntity> getNodeFormList(Integer pageIndex,Integer pageSize,String orderBy,String filter);
	/**
	 * 根据类型获取默认的form表单
	 * @param formType 可以从NodeFormEntity中获取
	 * @return
	 */
	NodeFormEntity getDefaultNodeForm(Integer formType);
	/**
	 * 更新字段信息
	 * @param formField
	 */
	void updateFieldInfo(NodeFormFieldEntity formField);
	/**
	 * 添加一个新的nodeForm，这个form应该 包含了nodeID且不做node-nodeForm对照
	 * @param nodeId
	 * @param nodeForm
	 */
	void addNewNodeForm(Long  nodeId,NodeFormEntity nodeForm,Integer type);
	/**
	 * 更新nodeForm基本信息
	 * @param nodeForm
	 */
	void updateNodeFormBaseInfo(NodeFormEntity nodeForm);
	
	NodeFormExtEntity getDefaultNodeFormExtEntity(Integer formType);
	
}
