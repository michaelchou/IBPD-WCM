package com.ibpd.henuocms.service.role;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.henuocms.entity.RoleEntity;
@Transactional
@Service("roleService")
public class RoleServiceImpl extends BaseServiceImpl<RoleEntity> implements IRoleService {
	public RoleServiceImpl(){
		super();
		this.tableName="RoleEntity";
		this.currentClass=RoleEntity.class;
		this.initOK();
	}
}
 