package com.ibpd.henuocms.assist;
import org.springframework.beans.propertyeditors.PropertiesEditor;
/**
 * spring的自定义prop处理器
 * @author mg by qq:349070443
 *
 */
public class BooleanEditor extends PropertiesEditor {  
    @Override  
    public void setAsText(String text) throws IllegalArgumentException {  
        if (text == null || text.equals("") || text.equals("0") || text.trim().toLowerCase().equals("false")) {  
            text = "false";  
        }else{
        	text="true";
        }
        setValue(Boolean.parseBoolean(text));  
    }  
  
    @Override  
    public String getAsText() {  
        return getValue().toString();  
    }  
    public static void main(String[] args){
    	String s="true";
    	System.out.println(Boolean.parseBoolean(s));
    }
}  