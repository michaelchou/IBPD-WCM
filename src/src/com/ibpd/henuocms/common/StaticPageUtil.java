package com.ibpd.henuocms.common;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.ibpd.henuocms.exception.IbpdException;


/**
 * 生成静态页面


 * @author mg
 *
 */
public class StaticPageUtil {
	/**
	 * 将特定的url地址以及参数等转换为具体的HTML文件到具体目录
	 **/
	public void writeHtmlToFile(StaticPageEntity sp) throws Exception{
		writeHtmlToFile(sp.getUrl(),sp.getUrlParams(),sp.getFileName());
	}
	/**
	 * 将特定的url地址以及参数等转换为具体的HTML文件到具体目录
	 **/
	public void writeHtmlToFile(String url,Map<String,String> params,String fileName) throws Exception{
		if(url==null || fileName==null){
			throw new IbpdException("url或fileName参数为空");
		}
		DefaultHttpClient httpclient=new DefaultHttpClient();		
//		httpclient.setCookieStore(cookie);
		try {
			url=url+getParamsString(params);
			HttpGet httpget = new HttpGet(url);
			IbpdLogger.getLogger(this.getClass()).info("executing request " + httpget.getURI());
			httpget.setHeader("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			httpget.setHeader("Accept-Encoding","gzip, deflate");
			httpget.setHeader("Accept-Language","zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");
			httpget.setHeader("Connection","keep-alive");
//			httpget.setHeader("Referer","http://192.105.128.201/userpass.aspx?type=1");
			httpget.setHeader("User-Agent","Mozilla/5.0 (Windows NT 6.3; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0");
			CloseableHttpResponse response = httpclient.execute(httpget);
			try {
				HttpEntity entity = response.getEntity();
				IbpdLogger.getLogger(this.getClass()).info("--------------------------------------");
				IbpdLogger.getLogger(this.getClass()).info(response.getStatusLine());
				if (entity != null) {
					IbpdLogger.getLogger(this.getClass()).info("Response content length: " + entity.getContentLength());
					String cnt=EntityUtils.toString(entity);
					writeHtmlToFile(cnt,fileName);
				}
				IbpdLogger.getLogger(this.getClass()).info("------------------------------------");
			} finally {
				response.close();
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			httpclient.close();
		}

	}
	/**
	 * 写入html内容到具体文件，该函数有重载
	 * @param content
	 * @param fileDirName
	 * @throws Exception
	 */
	public void writeHtmlToFile(String content,String fileDirName) throws Exception{
		File f=new File(fileDirName);
		if(!f.exists()){
			f.createNewFile();
		}
		if(!f.canWrite()){
			throw new IOException(f.getAbsolutePath()+"文件不可写");
		}
		TxtUtil.writeTxtFile(content, new File(fileDirName));
	}
	/** 
	 * 获取get方式中url后面的参数
	 * @param pams
	 * @return
	 */
	private String getParamsString(Map<String,String> pams){
		if(pams==null)
			return "";
		Iterator<String> keys=pams.keySet().iterator();
		String pmString="?";
		while(keys.hasNext()){
			String key=keys.next();
			pmString+=key+"="+pams.get(key)+"&";
		}
		if(pmString.equals("?")){
			return "";
		}else{
			return pmString.substring(0,pmString.length()-1);
		}
	}
	public static void writeHtmlToHtmlFile(StaticPageEntity sp){
		StaticPageUtil util=new StaticPageUtil();
		try {
			util.writeHtmlToFile(sp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void main(String[] args){
		StaticPageUtil u=new StaticPageUtil();
		try {
			u.writeHtmlToFile("http://localhost:8080/henuoCMS/Manage/index.do",null,"c:\\1.html");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
