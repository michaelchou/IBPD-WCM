package com.ibpd.henuocms.common;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
/**
 * 利用SWFTools工具将pdf转换成swf，转换完后的swf文件与pdf同名<br />
 * 这个工具暂时用来生成电子杂志的swf文件，仅仅是电子杂志生成步骤中的一步<br />
 * 该工具类暂时应该是不支持pdf内嵌中文的文件转换的<br />
 * 该工具类同样支持将jpg图像格式的文件转换成swf格式
 * @author mg by qq:349070443
 *
 */
public class PDF2SWFUtil {
     
    /**
      * 利用SWFTools工具将pdf转换成swf，转换完后的swf文件与pdf同名<br />
      * 这个工具暂时用来生成电子杂志的swf文件，仅仅是电子杂志生成步骤中的一步<br />
      * 该工具类暂时应该是不支持pdf内嵌中文的文件转换的<br />
      * 改工具类同样支持将jpg图像格式的文件转换成swf格式
       * @author mg
      * @param fileDir PDF或jpg文件存放路径（包括文件名）
       * @param exePath 转换器安装路径
       * @throws IOException
      */
    public static synchronized void pdf2swf(String fileDir, String exePath) throws IOException {
        //文件路径
         String filePath = fileDir.substring(0, fileDir.lastIndexOf("/"));
        //文件名，不带后缀
         String fileName = fileDir.substring((filePath.length() + 1), fileDir.lastIndexOf("."));
        Process pro = null;
        if (isWindowsSystem()) {
            //如果是windows系统
              //命令行命令
              String cmd = exePath + " \"" + fileDir + "\" -o \"" + filePath + "/" + fileName + ".swf";
            //Runtime执行后返回创建的进程对象
              pro = Runtime.getRuntime().exec(cmd);
        } else {
            //如果是linux系统,路径不能有空格，而且一定不能用双引号，否则无法创建进程
              String[] cmd = new String[3];
            cmd[0] = exePath;
            cmd[1] = fileDir;
            cmd[2] = filePath + "/" + fileName + ".swf";
            //Runtime执行后返回创建的进程对象
              pro = Runtime.getRuntime().exec(cmd);
        }
        //非要读取一遍cmd的输出，要不不会flush生成文件（多线程）
         new DoOutput(pro.getInputStream()).start();
        new DoOutput(pro.getErrorStream()).start();
        try {
            //调用waitFor方法，是为了阻塞当前进程，直到cmd执行完
             pro.waitFor();
        } catch (InterruptedException e) {
           e.printStackTrace();
        }
    }
    private static String getJpg2swfExePath(){
   	 File fl=new File(PDF2SWFUtil.class.getResource("/").getPath());
   	 String path=fl.getParentFile().getAbsolutePath()+File.separatorChar+"SWFTools"+File.separatorChar+"jpeg2swf.exe";
   	IbpdLogger.getLogger(PDF2SWFUtil.class).info(path);
   	 return path;
//    	return "D:/Program Files (x86)/SWFTools/jpeg2swf.exe";
    }
    public static void jpg2swf(String jpgFilePath,String  swfFilePath) throws IOException{
       Process pro = null;
       String exePath="";
       if (isWindowsSystem()) {
           //如果是windows系统
             //命令行命令
    	   	exePath=getJpg2swfExePath();
            String cmd = exePath + " \"" + jpgFilePath + "\" -o \"" + swfFilePath+"\"";
           //Runtime执行后返回创建的进程对象
            pro = Runtime.getRuntime().exec(cmd);
       } else {
           //如果是linux系统,路径不能有空格，而且一定不能用双引号，否则无法创建进程
             String[] cmd = new String[3];
           cmd[0] = exePath;
           cmd[1] = jpgFilePath;
           cmd[2] = swfFilePath;
           //Runtime执行后返回创建的进程对象
             pro = Runtime.getRuntime().exec(cmd);
       }
       //非要读取一遍cmd的输出，要不不会flush生成文件（多线程）
        new DoOutput(pro.getInputStream()).start();
       new DoOutput(pro.getErrorStream()).start();
       try {
           //调用waitFor方法，是为了阻塞当前进程，直到cmd执行完
            pro.waitFor();
       } catch (InterruptedException e) {
          e.printStackTrace();
       }
   	
    }
     
    /**
      * 判断是否是windows操作系统
       * @author iori
      * @return
      */
    private static boolean isWindowsSystem() {
        String p = System.getProperty("os.name");
        return p.toLowerCase().indexOf("windows") >= 0 ? true : false;
    }
     
    /**
      * 多线程内部类
       * 读取转换时cmd进程的标准输出流和错误输出流，这样做是因为如果不读取流，进程将死锁
       * @author iori
      */
    private static class DoOutput extends Thread {
        public InputStream is;
      
        //构造方法
         public DoOutput(InputStream is) {
            this.is = is;
        }
      
        public void run() {
            BufferedReader br = new BufferedReader(new InputStreamReader(this.is));
            String str = null;
            try {
                //这里并没有对流的内容进行处理，只是读了一遍
                  while ((str = br.readLine()) != null);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
     /** 
      * 测试main方法
       * @param args
      */
    public static void main(String[] args) {
        //转换器安装路径
         String exePath = "D:/Program Files (x86)/SWFTools/pdf2swf.exe";
        try {
            PDF2SWFUtil.pdf2swf("H:/1.pdf", exePath);
            System.out.println("success");
        } catch (IOException e) {
            System.err.println("转换出错！");
            e.printStackTrace();
        }
    }
}