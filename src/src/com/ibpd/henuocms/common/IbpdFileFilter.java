package com.ibpd.henuocms.common;

import java.io.File;
import java.io.FileFilter;
/**
 * 自定义文件过滤器 列表显示文件的时候使用
 * @author mg by qq:349070443
 *
 */
public class IbpdFileFilter implements FileFilter {
 
	private String[] fileExts=new String[]{};
	public  IbpdFileFilter(String[] exts){
		fileExts=exts;
	}
	public boolean accept(File pathname) {
		if (pathname.isDirectory())
			return true;
		if(fileExts.length==0)
			return true;
		else {
			String name = pathname.getName();
			Boolean rtn=false;
			for(String ext:fileExts){
				if (name.endsWith(ext)){
					rtn = true;
					break;
				}
			}
			return rtn;
		}

	}

}
