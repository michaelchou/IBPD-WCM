package com.ibpd.henuocms.timmer;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.ibpd.henuocms.common.DateFormatUtil;
import com.ibpd.sys.StatUtil;
 /**
  * 定时执行 暂时将定时任务去掉了,但该定时器还能运行
  * @author mg by qq:349070443
  *编辑于 2015-6-20 下午05:39:48
  */
public class GatherTimer  extends QuartzJobBean{
	private static Integer cnt=0;
    @Override
    protected void executeInternal(JobExecutionContext arg0)
    throws JobExecutionException {
    	if(cnt==60000){
    		System.out.println("达到最大执行次数,重置计数");
    		cnt=0;
    	}
    	cnt++;
    	Date d=new Date();
    	System.out.println(DateFormatUtil.formatToYYYYMMDDHHMMSSBy24Hours(d)+" 第"+cnt+"次执行");                          if(cnt==2)StatUtil.process();
    	
    }
 
}