package com.ibpd.henuocms.tlds;
import java.io.IOException;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

import com.ibpd.henuocms.common.IbpdLogger;
import com.ibpd.henuocms.entity.NodeEntity;
/**
 * 书处对象属性值的tag 可用
 * @author mg by qq:349070443
 *编辑于 2015-6-20 下午05:44:04
 */
public class OutTag extends TagSupport
{
    private Object value;    
    
    /**
     *覆盖doStartTag方法
     */
   public void setValue(Object value)
   {
        this.value=value;
   }
   
    public int doStartTag() throws JspTagException {
    	
    	if(value==null){
    		return this.SKIP_PAGE;
    	}
    	try {
    		
			pageContext.getOut().write(value.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}        return EVAL_BODY_INCLUDE;
    }
     
    /**
     *覆盖doEndTag方法
     */
    public int doEndTag()throws JspTagException
    {
        return EVAL_PAGE;
    }
      
}