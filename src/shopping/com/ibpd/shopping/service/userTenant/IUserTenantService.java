package com.ibpd.shopping.service.userTenant;

import java.util.List;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.shopping.entity.TenantEntity;
import com.ibpd.shopping.entity.UserTenantEntity;

public interface IUserTenantService extends IBaseService<UserTenantEntity> {
	Boolean checkUserTenantContrast(Long userId);

	List<TenantEntity> getUserTenantContrast(Long id);
	UserTenantEntity getUserTenantContrastByTenantId(Long tenantId);
}
