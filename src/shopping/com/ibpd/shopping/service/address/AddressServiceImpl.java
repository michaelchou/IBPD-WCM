package com.ibpd.shopping.service.address;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.dao.impl.HqlParameter;
import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.shopping.entity.AccountEntity;
import com.ibpd.shopping.entity.AddressEntity;
import com.ibpd.shopping.entity.AreaEntity;
import com.ibpd.shopping.service.account.AccountServiceImpl;
import com.ibpd.shopping.service.account.IAccountService;
import com.ibpd.shopping.service.area.AreaServiceImpl;
import com.ibpd.shopping.service.area.IAreaService;
@Service("addressService")
public class AddressServiceImpl extends BaseServiceImpl<AddressEntity> implements IAddressService {
	public AddressServiceImpl(){
		super();
		this.tableName="AddressEntity";
		this.currentClass=AddressEntity.class;
		this.initOK();
	}

	public List<AddressEntity> getAccountAddressList(String account) {
		if(StringUtils.isBlank(account)){
			return null;
		}
		String jpql="from "+this.getTableName()+" where account=:account";
		List<HqlParameter> hqlParameterList=new ArrayList<HqlParameter>();
		hqlParameterList.add(new HqlParameter("account",account,HqlParameter.DataType_Enum.String));
		return this.getList(jpql, hqlParameterList);
	}

	public void initDemoData() {
		IAccountService as=(IAccountService) ServiceProxyFactory.getServiceProxy(AccountServiceImpl.class);
		List<AccountEntity> al=as.getList();
		IAreaService areaServ=(IAreaService) ServiceProxyFactory.getServiceProxy(AreaServiceImpl.class);
		for(Integer i=0;i<al.size();i++){
			AccountEntity a=al.get(i);
			AreaEntity area_prov=areaServ.getAreaByCode("140000");
			AreaEntity area_city=areaServ.getAreaByCode("140100");
			AreaEntity area_area=areaServ.getAreaByCode("140105");
			AddressEntity add=new AddressEntity();
			add.setAccount(a.getAccount());
			add.setProvince(area_prov.getName());
			add.setCity(area_city.getName());
			add.setArea(area_area.getName());
			add.setAddress("南中环街 路南 XXX小区 2栋三单元2101号");
			add.setZip("030006");
			add.setIsdefault("y");
			add.setMobile("1345316883"+i);
			add.setName("收货人姓名"+i);
			add.setPcadetail(add.getProvince()+add.getCity()+add.getArea());
			add.setPhone("1345316883"+i);
			this.saveEntity(add);
		}
	}
}
