package com.ibpd.shopping.service.area;

import java.util.List;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.henuocms.exception.IbpdException;
import com.ibpd.shopping.entity.AreaEntity;

public interface IAreaService extends IBaseService<AreaEntity> {
	String getAreaNameByCode(String areaCode) throws IbpdException;
	List<AreaEntity> getAreaListByParentCode(String pCode);
	AreaEntity getAreaByCode(String code);
	AreaEntity getAreaByName(String string) throws IbpdException;
}
