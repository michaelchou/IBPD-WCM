package com.ibpd.shopping.service.orderpay;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.shopping.entity.OrderpayEntity;
@Service("orderpayService")
public class OrderpayServiceImpl extends BaseServiceImpl<OrderpayEntity> implements IOrderpayService {
	public OrderpayServiceImpl(){
		super();
		this.tableName="OrderpayEntity";
		this.currentClass=OrderpayEntity.class;
		this.initOK();
	}

	public OrderpayEntity getOrderpayByOrderId(Long orderId) {
		
		List<OrderpayEntity> lst=getList("from "+getTableName()+" where orderid="+orderId,null);
		if(lst==null || lst.size()==0)
			return null;
		else
			return lst.get(0);
	}

	public void deleteByOrderId(Long orderId) {
		OrderpayEntity o=getOrderpayByOrderId(orderId);
		if(o!=null)
		this.deleteByPK(o.getId());
		
	}
}
