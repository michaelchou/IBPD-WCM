package com.ibpd.shopping.service.order;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.dao.impl.HqlParameter;
import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.shopping.entity.OrderEntity;
import com.ibpd.shopping.entity.OrderLogEntity;
import com.ibpd.shopping.entity.OrderShipEntity;
import com.ibpd.shopping.entity.OrderdetailEntity;
import com.ibpd.shopping.entity.OrderpayEntity;
import com.ibpd.shopping.service.orderDetail.IOrderdetailService;
import com.ibpd.shopping.service.orderDetail.OrderdetailServiceImpl;
import com.ibpd.shopping.service.orderShip.IOrderShipService;
import com.ibpd.shopping.service.orderShip.OrderShipServiceImpl;
import com.ibpd.shopping.service.orderlog.IOrderLogService;
import com.ibpd.shopping.service.orderlog.OrderLogServiceImpl;
import com.ibpd.shopping.service.orderpay.IOrderpayService;
import com.ibpd.shopping.service.orderpay.OrderpayServiceImpl;
@Service("orderService") 
public class OrderServiceImpl extends BaseServiceImpl<OrderEntity> implements IOrderService {
	Logger logger=Logger.getLogger(OrderServiceImpl.class);
	public OrderServiceImpl(){
		super();
		this.tableName="OrderEntity";
		this.currentClass=OrderEntity.class;
		this.initOK();
	}
	
	@Override
	public int saveEntity(OrderEntity entity) {
		entity.setUpdatedate(new Date());
		return super.saveEntity(entity);
	}

	public boolean createOrder(OrderEntity order, List<OrderdetailEntity> orderdetailList,
			OrderShipEntity ordership) throws Exception {
		if (order == null || orderdetailList == null
				|| orderdetailList.size() == 0 || ordership == null) {
			throw new NullPointerException("参数不能为空！");
		}

		// 对商品进行砍库存，并同步内存中的库存数据
		// if(!no){
		// //如果检查没有出现库存不足的情况，则进行砍库存操作
		// for (int i = 0; i < cartInfo.getProductList().size(); i++) {
		// Product product = cartInfo.getProductList().get(i);
		// ProductStockInfo stockInfo =
		// SystemManager.productStockMap.get(product.getId());
		// stockInfo.setStock(stockInfo.getStock() - product.getBuyCount());
		// stockInfo.setChangeStock(true);
		// SystemManager.productStockMap.put(product.getId(),stockInfo);
		// }
		// }

		// 创建订单
		saveEntity(order);
		logger.debug("orderID=" + order.getId());

		// 创建订单项
		IOrderdetailService ds=(IOrderdetailService) ServiceProxyFactory.getServiceProxy(OrderdetailServiceImpl.class);
		for (int i = 0; i < orderdetailList.size(); i++) {
			OrderdetailEntity orderdetail = orderdetailList.get(i);
			orderdetail.setOrderID(order.getId());
			ds.saveEntity(orderdetail);
		}

		// 创建支付记录对象
		IOrderpayService orderpayServ=(IOrderpayService) ServiceProxyFactory.getServiceProxy(OrderpayServiceImpl.class);
		OrderpayEntity orderpay = new OrderpayEntity();
		orderpay.setOrderid(order.getId().toString());
		orderpay.setPaystatus(OrderpayEntity.orderpay_paystatus_n);
		orderpay.setPayamount(Double.valueOf(order.getAmount()));
		orderpay.setPaymethod(OrderpayEntity.orderpay_paymethod_alipayescow);
		orderpayServ.saveEntity(orderpay);
		logger.error("orderpayID=" + orderpay.getId());
//		order.setOrderpayID(String.valueOf(orderpay.getId()));

		// 记录配送信息
		IOrderShipService ordershipServ=(IOrderShipService)ServiceProxyFactory.getServiceProxy(OrderShipServiceImpl.class);
		ordership.setOrderid(order.getId());
		ordershipServ.saveEntity(ordership);

		// 记录订单创建日志
		OrderLogEntity orderlog = new OrderLogEntity();
		orderlog.setOrderid(String.valueOf(order.getId()));
		orderlog.setAccount(order.getAccount());
		orderlog.setContent("【创建订单】用户创建订单。订单总金额：" + order.getAmount());
		orderlog.setAccountType(OrderLogEntity.orderlog_accountType_w);
		IOrderLogService orderLogServ=(IOrderLogService)ServiceProxyFactory.getServiceProxy(OrderLogServiceImpl.class);
		orderLogServ.saveEntity(orderlog);
		return true;
	}

	public List<OrderEntity> getListByAccount(String account,Integer pageSize,Integer pageIndex) {
		return this.getList("from "+getTableName()+" where account='"+account+"'", null, "createdate desc", pageSize, pageIndex);
	}

	public List<OrderEntity> getRangeOrderList(Date startDate, Date endDate) {
		if(startDate!=null && endDate!=null){
			endDate.setHours(23);
			endDate.setMinutes(59);
			endDate.setSeconds(59);
			Criteria  criteria = this.getDao().getCurrentSession().createCriteria(OrderEntity.class);
			   criteria.add(Restrictions.ge("createdate",startDate)); 
			   criteria.add(Restrictions.le("createdate",endDate));
//				criteria.add(Restrictions.between("createdate", startDate, endDate));
			   criteria.add(Restrictions.eq("status", OrderEntity.order_status_file));
			   List<OrderEntity> list = criteria.list();
			   return list;
//			List<HqlParameter> param=new ArrayList<HqlParameter>();
//			param.add(new HqlParameter("dt1",startDate,HqlParameter.DataType_Enum.Date));
//			param.add(new HqlParameter("dt2",endDate,HqlParameter.DataType_Enum.Date));
//			return getList("from "+getTableName()+" where createdate between :dt1 and :dt2 and status='"+OrderEntity.order_status_file+"'",param);
		}else{
			return null;
		}
	}

	public List<OrderEntity> getListByAccount(String account) {
		// TODO Auto-generated method stub
		return null;
	} 

}
