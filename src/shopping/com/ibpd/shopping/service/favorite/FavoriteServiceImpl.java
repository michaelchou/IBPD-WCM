package com.ibpd.shopping.service.favorite;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.dao.impl.HqlParameter;
import com.ibpd.shopping.entity.FavoriteEntity;
@Service("favoriteService")
public class FavoriteServiceImpl extends BaseServiceImpl<FavoriteEntity> implements IFavoriteService {
	public FavoriteServiceImpl(){
		super();
		this.tableName="FavoriteEntity";
		this.currentClass=FavoriteEntity.class;
		this.initOK();
	}

	public Integer selectCount(FavoriteEntity fe) {
		String hql="from "+getTableName()+" where accId=:account and productId=:productId";
		List<HqlParameter> hqlList=new ArrayList<HqlParameter>();
		hqlList.add(new HqlParameter("account",fe.getAccId(),HqlParameter.DataType_Enum.Long));
		hqlList.add(new HqlParameter("productId",fe.getProductId(),HqlParameter.DataType_Enum.String));
		List lst=getList(hql,hqlList);
		if(lst==null)
			return 0;
		else
			return lst.size();
	}

	public List<FavoriteEntity> getFavoriteListByAccount(Long accountId) {
		String hql="from "+getTableName()+" where accId=:account";
		List<HqlParameter> hqlList=new ArrayList<HqlParameter>();
		hqlList.add(new HqlParameter("account",accountId,HqlParameter.DataType_Enum.Long));
		List<FavoriteEntity> lst=getList(hql,hqlList,"id desc",1000,0);
		return lst;
	}
	public void removeFavoriteByProductId(Long accountId,Long productId) {
		String hql="from "+getTableName()+" where accId=:account and productId=:productId";
		List<HqlParameter> hqlList=new ArrayList<HqlParameter>();
		hqlList.add(new HqlParameter("account",accountId,HqlParameter.DataType_Enum.Long));
		hqlList.add(new HqlParameter("productId",productId,HqlParameter.DataType_Enum.Long));
		List<FavoriteEntity> lst=getList(hql,hqlList);
		if(lst!=null && lst.size()>0){
			for(FavoriteEntity fav:lst){
				this.deleteByPK(fav.getId());
			}
		}
	}

	public void removeFavoriteByTenantId(Long accountId, Long tenantId) {
		String hql="from "+getTableName()+" where accId=:account and tenantId=:tenantId";
		List<HqlParameter> hqlList=new ArrayList<HqlParameter>();
		hqlList.add(new HqlParameter("account",accountId,HqlParameter.DataType_Enum.Long));
		hqlList.add(new HqlParameter("tenantId",tenantId,HqlParameter.DataType_Enum.Long));
		List<FavoriteEntity> lst=getList(hql,hqlList);
		if(lst!=null && lst.size()>0){
			for(FavoriteEntity fav:lst){
				this.deleteByPK(fav.getId());
			}
		}
	}
}
