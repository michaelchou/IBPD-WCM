
package com.ibpd.shopping.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import net.sf.json.JSONArray;

import com.ibpd.entity.baseEntity.IBaseEntity;

@Entity
@Table(name="t_s_address")
public class AddressEntity extends IBaseEntity{
	
	private static final long serialVersionUID = 1L;
	
	@Id @Column(name="f_id",nullable=true) @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="f_account",length=45,nullable=true)
	private String account=null;
	
	@Column(name="f_name",length=45,nullable=true)
	private String name=null;
	
	@Column(name="f_address",length=245,nullable=true)
	private String address=null;
	
	@Column(name="f_zip",length=6,nullable=true)
	private String zip=null;
	
	@Column(name="f_phone",length=25,nullable=true)
	private String phone=null;
	
	@Column(name="f_mobile",length=25,nullable=true)
	private String mobile=null;
	
	@Column(name="f_isdefault",length=2,nullable=true)
	private String isdefault="n";
	
	@Column(name="f_province",length=15,nullable=true)
	private String province=null;
	
	@Column(name="f_city",length=15,nullable=true)
	private String city=null;
	
	@Column(name="f_area",length=15,nullable=true)
	private String area=null;
	
	@Column(name="f_pcadetail",length=75,nullable=true)
	private String pcadetail=null;
	
	@Column(name="f_freeze",length=2,nullable=true)
	private String freeze="n";
	
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_addDate",nullable=true)
	private Date addDate=new Date();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getIsdefault() {
		return isdefault;
	}

	public void setIsdefault(String isdefault) {
		this.isdefault = isdefault;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getPcadetail() {
		return pcadetail;
	}

	public void setPcadetail(String pcadetail) {
		this.pcadetail = pcadetail;
	}

	public String getFreeze() {
		return freeze;
	}

	public void setFreeze(String freeze) {
		this.freeze = freeze;
	}

	public Date getAddDate() {
		return addDate;
	}

	public void setAddDate(Date addDate) {
		this.addDate = addDate;
	}

	@Override
	public String toString() {
		JSONArray j=JSONArray.fromObject(this);
		return j.toString();
	}
}
